## ImageCycleView
**实现图片自动循环的组件**

#### 使用
**Gradle**
```
compile 'com.weicong.library:imagecycleview:1.0.2'
```
**布局文件**
```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.weicong.cycleviewpager.MainActivity">

    <com.weicong.library.ImageCycleView
        android:id="@+id/imageCycle"
        android:layout_width="match_parent"
        android:layout_height="200dp"
        />

</RelativeLayout>
```

**Java代码**

**1.显示本地资源图片**
```java
ArrayList<Integer> imageRes = new ArrayList<>();
// 图片资源id
imageRes.add(R.drawable.image1);
imageRes.add(R.drawable.image2);
imageRes.add(R.drawable.image3);
imageRes.add(R.drawable.image4);
imageRes.add(R.drawable.image5);
imageCycleView.setImageRes(imageRes);

// 开始自动循环
imageCycleView.start();
// 停止循环
// imageCycleView.stop();
```
**注：如果不调用start()方法不会自动跳到下一张图片，但可以手动滑到下一张图片。具体可参考CycleViewPager类——实现了可循环滑动的ViewPager。**

**效果图：**

![效果图](image/image1.png)

**2.显示网络图片**
```java
ArrayList<String> imageUrls = new ArrayList<>();
// 图片url
imageUrls.add("http://img.my.csdn.net/uploads/201404/13/1397393290_5765.jpeg");
imageUrls.add("http://img2.imgtn.bdimg.com/it/u=2696993843,2364483949&fm=21&gp=0.jpg");
imageUrls.add("http://img4.imgtn.bdimg.com/it/u=4270507479,3433421651&fm=21&gp=0.jpg");
imageCycleView.setImageUrl(imageUrls);

// 开始自动循环
imageCycleView.start();
```
**注：加载图片使用[Glide](https://github.com/bumptech/glide)**

**效果图：**

![效果图](image/image2.png)

#### 自定义配置
```java
ImageCycleView.Config config = new ImageCycleView.Config()
           .setIndicatorPos(ImageCycleView.RIGHT) // 设置指示符的位置（居中或居右）
           .setIndicatorNormalRes(R.drawable.indicator_normal) // 没选中的指示符样式
           .setIndicatorSelectRes(R.drawable.indicator_select) // 当前选中的指示符的样式
           .setIndicatorMargin(5) // 设置指示符之间的间隔
           .setBottomMargin(10) // 设置指示符离底部的边距
           .setRightMargin(12) // 设置指示符离右边的边距（当指示符居右时）
           .setPlaceHolder(R.drawable.placeholder) // 设置加载网络图片时的占位图片资源id
           .setTime(5000) // 设置自动跳转的时间间隔，单位毫秒
           .setCurrentIndex(2) // 设置从第几项开始
           // 设置item单击监听器
           .setOnItemClickListener(new ImageCycleView.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Toast.makeText(MainActivity.this, "position -> " + position, Toast.LENGTH_SHORT).show();
                }
            });
// 应用自定义配置
imageCycleView.setConfig(config);
```